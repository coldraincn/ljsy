package com.coldraincn.ljsy.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.coldraincn.ljsy.R;
import com.coldraincn.ljsy.bean.Course;

import java.util.List;

/**
 * Created by Administrator on 2017/9/22 0022.
 */

public class PastCourseAdapter extends CommonAdapter {

    public PastCourseAdapter(Context context, List<Course> mDatas) {
        super(context, mDatas);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup viewGroup) {
        final ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = mInflater.inflate(R.layout.list_item_past_course, null);
            holder.iv_img = convertView.findViewById(R.id.iv_img);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        return convertView;
    }

    private static final class ViewHolder {
        ImageView iv_img;
    }

}
