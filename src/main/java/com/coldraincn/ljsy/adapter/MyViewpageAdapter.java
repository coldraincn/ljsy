package com.coldraincn.ljsy.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

public class MyViewpageAdapter extends FragmentPagerAdapter {
	
	private List<Fragment> fragmentList = new ArrayList<Fragment>();
	private FragmentManager fm;
	
	
	
	public MyViewpageAdapter(FragmentManager fm, List<Fragment> list) {
		super(fm);
		this.fm = fm;
		fragmentList = list;
	}

	@Override
	public Fragment getItem(int arg0) {

		return fragmentList.get(arg0);
	}

	@Override
	public int getCount() {

		return fragmentList.size();
	}

	public void setFragments(List<Fragment> fragments){
		if(this.fragmentList != null){
			android.support.v4.app.FragmentTransaction ft = fm.beginTransaction();
			for(Fragment f : this.fragmentList){
				ft.remove(f);
			}
			ft.commit();
			ft = null;
			fm.executePendingTransactions();
		}
		this.fragmentList = fragments;
		notifyDataSetChanged();
	}
	
	
}
