package com.coldraincn.ljsy.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.coldraincn.ljsy.R;
import com.coldraincn.ljsy.bean.Course;

import java.util.List;

/**
 * Created by Administrator on 2017/9/22 0022.
 */

public class CourseAdapter extends CommonAdapter {
    private boolean isMine;
    private  List<Course> mDatas1;

    public CourseAdapter(Context context, List<Course> mDatas, boolean isMine) {
        super(context, mDatas);
        this.isMine = isMine;
        mDatas1=mDatas;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup viewGroup) {
        final ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = mInflater.inflate(R.layout.list_item_course, null);
            holder.tv_className = convertView.findViewById(R.id.className);
            holder.tv_content = convertView.findViewById(R.id.content);
            holder.iv_img = convertView.findViewById(R.id.iv_img);
            holder.tv_price = convertView.findViewById(R.id.tv_price);
            holder.tv_dingyue = convertView.findViewById(R.id.tv_dingyue);

            holder.tv_price.setText(mDatas1.get(position).getPrice());
            holder.tv_className.setText(mDatas1.get(position).getClassNeme());
            holder.tv_content.setText(mDatas1.get(position).getContent());
            if(mDatas1.get(position).getImg().equals("111")){
                holder.iv_img.setImageResource(R.drawable.aaa);
            }else{
                holder.iv_img.setImageResource(R.drawable.bbb);
            }
            if (isMine) {
                holder.tv_dingyue.setText("学习此课程");
                holder.tv_price.setVisibility(View.GONE);
            }
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
//        if (isMine) {
//            holder.tv_dingyue.setText("学习此课程");
//            holder.tv_price.setVisibility(View.GONE);
//        } else {
//
//        }
        return convertView;
    }

    private static final class ViewHolder {
        ImageView iv_img;
        TextView tv_price;
        TextView tv_dingyue;
        TextView tv_className;
        TextView tv_content;
    }

}
