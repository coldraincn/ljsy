package com.coldraincn.ljsy.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;

import com.coldraincn.ljsy.R;
import com.coldraincn.ljsy.util.ToastUtils;

public class DiscussFragment extends Fragment implements OnClickListener  {
    private final String TAG = "FirstPageFragment";
    private View mainView;
    private Activity activity;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = getActivity();
        LayoutInflater inflater = activity.getLayoutInflater();
        mainView = inflater.inflate(R.layout.fragment_discuss, (ViewGroup) activity.findViewById(R.id.mypagers_pager), false);

    }




    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ViewGroup p = (ViewGroup) mainView.getParent();
        if (p != null) {
            p.removeAllViewsInLayout();
        }
        return mainView;
    }

    private void initUI() {



    }





    private void toastMsg(String msg) {
        ToastUtils.show(activity, msg);
    }


    @Override
    public void onClick(View view) {

    }
}
