package com.coldraincn.ljsy.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.coldraincn.ljsy.CourseDetailActivity;
import com.coldraincn.ljsy.R;
import com.coldraincn.ljsy.adapter.CourseAdapter;
import com.coldraincn.ljsy.bean.Course;
import com.coldraincn.ljsy.util.ToastUtils;

import java.util.ArrayList;
import java.util.List;

public class HomeFragment extends Fragment implements OnClickListener {
    private final String TAG = "FirstPageFragment";
    private View mainView;
    private View btn_tjkc;
    private View btn_wdkc;
    private Activity activity;
    private TextView tv_tjkc;
    private TextView tv_wdkc;
    private ImageView iv_tjkc;
    private ImageView iv_wdkc;
    private ListView listView;
    private List<Course> coursesTJ = new ArrayList<>();
    private boolean isMine = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = getActivity();
        LayoutInflater inflater = activity.getLayoutInflater();
        mainView = inflater.inflate(R.layout.fragment_home, (ViewGroup) activity.findViewById(R.id.mypagers_pager), false);
        initUI();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ViewGroup p = (ViewGroup) mainView.getParent();
        if (p != null) {
            p.removeAllViewsInLayout();
        }
        return mainView;
    }

    private void initUI() {
        btn_tjkc = mainView.findViewById(R.id.btn_tjkc);
        btn_wdkc = mainView.findViewById(R.id.btn_wdkc);
        tv_tjkc = mainView.findViewById(R.id.tv_tjkc);
        tv_wdkc = mainView.findViewById(R.id.tv_wdkc);
        iv_tjkc = mainView.findViewById(R.id.iv_tjkc);
        iv_wdkc = mainView.findViewById(R.id.iv_wdkc);
        listView = mainView.findViewById(R.id.listView);
        btn_tjkc.setOnClickListener(this);
        btn_wdkc.setOnClickListener(this);
        Course course1=new Course();
        course1.setClassNeme("人际法则：10分钟国学课");
        course1.setContent("首次问世，从未公开发行。10分钟讲透孔孟先贤为人处世之道！");
        course1.setPrice("199");
        course1.setImg("111");
        Course course2=new Course();
        course2.setClassNeme("通往总裁之路");
        course2.setContent("当你莫名其妙被领导责骂，被同事排斥，被客户讨厌。当你付出很多，却收获……");
        course2.setPrice("199");
        course2.setImg("222");
        coursesTJ.add(course1);
        coursesTJ.add(course2);
//        for (int i = 0; i <2; i++) {
//            coursesTJ.add(null);
//        }
        listView.setAdapter(new CourseAdapter(activity, coursesTJ, false));
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(activity, CourseDetailActivity.class);
                intent.putExtra("isMine", isMine);
                startActivity(intent);
            }
        });
    }

    private void toastMsg(String msg) {
        ToastUtils.show(activity, msg);
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_tjkc:
                tv_tjkc.setTextColor(0xff33cccc);
                tv_wdkc.setTextColor(0xffffffff);
                iv_tjkc.setImageResource(R.drawable.kc_pressed);
                iv_wdkc.setImageResource(R.drawable.kc_unpress);
                isMine = false;
                listView.setAdapter(new CourseAdapter(activity, coursesTJ, false));
                break;
            case R.id.btn_wdkc:
                tv_tjkc.setTextColor(0xffffffff);
                tv_wdkc.setTextColor(0xff33cccc);
                iv_tjkc.setImageResource(R.drawable.kc_unpress);
                iv_wdkc.setImageResource(R.drawable.kc_pressed);
                isMine = true;
                listView.setAdapter(new CourseAdapter(activity, coursesTJ, true));
                break;
        }

    }
}
