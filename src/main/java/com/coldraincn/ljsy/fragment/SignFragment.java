package com.coldraincn.ljsy.fragment;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;

import com.coldraincn.ljsy.R;
import com.coldraincn.ljsy.util.ToastUtils;
import com.coldraincn.ljsy.view.OneDayDecorator;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.DayViewDecorator;
import com.prolificinteractive.materialcalendarview.DayViewFacade;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.prolificinteractive.materialcalendarview.OnDateSelectedListener;

public class SignFragment extends Fragment implements OnClickListener {
    private final String TAG = "FirstPageFragment";
    private View mainView;
    private Activity activity;
    private MaterialCalendarView calendarView;

    private final OneDayDecorator oneDayDecorator = new OneDayDecorator();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = getActivity();
        LayoutInflater inflater = activity.getLayoutInflater();
        mainView = inflater.inflate(R.layout.fragment_sign, (ViewGroup) activity.findViewById(R.id.mypagers_pager), false);
        initUI();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ViewGroup p = (ViewGroup) mainView.getParent();
        if (p != null) {
            p.removeAllViewsInLayout();
        }
        return mainView;
    }

    private void initUI() {
        calendarView = mainView.findViewById(R.id.calendarView);
//        calendarView.setSelectionColor(0xffff9640);
//        calendarView.addDecorators(new MySelectorDecorator(activity), oneDayDecorator);
    }

    public class MySelectorDecorator implements DayViewDecorator {

        private final Drawable drawable;

        public MySelectorDecorator(Activity context) {
            drawable = context.getResources().getDrawable(R.drawable.day_selector);
        }

        @Override
        public boolean shouldDecorate(CalendarDay day) {
            return true;
        }

        @Override
        public void decorate(DayViewFacade view) {
            view.setSelectionDrawable(drawable);
        }
    }

    private void toastMsg(String msg) {
        ToastUtils.show(activity, msg);
    }


    @Override
    public void onClick(View view) {

    }
}
