package com.coldraincn.ljsy;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.coldraincn.ljsy.util.ToastUtils;

public class BaseActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onClick(View view) {

    }

    public void back(View v) {
        finish();
    }

    public void toastMsg(String msg) {
        ToastUtils.show(this, msg);
    }
}
