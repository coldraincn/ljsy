package com.coldraincn.ljsy;

import android.graphics.Color;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.coldraincn.ljsy.adapter.MyViewpageAdapter;
import com.coldraincn.ljsy.fragment.DiscussFragment;
import com.coldraincn.ljsy.fragment.HomeFragment;
import com.coldraincn.ljsy.fragment.MyFragment;
import com.coldraincn.ljsy.fragment.SignFragment;

import android.support.v4.app.Fragment;

import java.util.ArrayList;

public class MainActivity extends BaseActivity {
    private View btn_home, btn_discuss, btn_sign, btn_my;
    private ImageView iv_home, iv_discuss, iv_sign, iv_my;
    private TextView tv_home, tv_discuss, tv_sign, tv_my;
    private ViewPager mPager;
    private int currIndex = 0;// 当前页卡编号

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btn_home = findViewById(R.id.btn_home);
        btn_discuss = findViewById(R.id.btn_discuss);
        btn_sign = findViewById(R.id.btn_sign);
        btn_my = findViewById(R.id.btn_my);
        btn_home.setOnClickListener(this);
        btn_discuss.setOnClickListener(this);
        btn_sign.setOnClickListener(this);
        btn_my.setOnClickListener(this);
        iv_home = (ImageView) findViewById(R.id.iv_home);
        iv_discuss = (ImageView) findViewById(R.id.iv_discuss);
        iv_sign = (ImageView) findViewById(R.id.iv_sign);
        iv_my = (ImageView) findViewById(R.id.iv_my);
        tv_home = (TextView) findViewById(R.id.tv_home);
        tv_discuss = (TextView) findViewById(R.id.tv_discuss);
        tv_sign = (TextView) findViewById(R.id.tv_sign);
        tv_my = (TextView) findViewById(R.id.tv_my);
        mPager = (ViewPager) findViewById(R.id.mypagers_pager);
        HomeFragment homeFragment = new HomeFragment();
        SignFragment signFragment = new SignFragment();
        DiscussFragment discussFragment = new DiscussFragment();
        MyFragment myFragment = new MyFragment();

        ArrayList<Fragment> listViews = new ArrayList<>();
        listViews.add(homeFragment);
        listViews.add(discussFragment);
        listViews.add(signFragment);
        listViews.add(myFragment);
        mPager.setAdapter(new MyViewpageAdapter(getSupportFragmentManager(), listViews));
        mPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            private int unPressedColor = 0xffaaaaaa;
            private int pressedColor = 0xff33cccc;

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                switch (currIndex) {//把原来的变成灰色
                    case 0:
                        iv_home.setImageDrawable(getResources().getDrawable(R.drawable.home));
                        tv_home.setTextColor(unPressedColor);
                        break;
                    case 1:
                        iv_discuss.setImageDrawable(getResources().getDrawable(R.drawable.discuss));
                        tv_discuss.setTextColor(unPressedColor);
                        break;
                    case 2:
                        iv_sign.setImageDrawable(getResources().getDrawable(R.drawable.sign));
                        tv_sign.setTextColor(unPressedColor);
                        break;
                    case 3:
                        iv_my.setImageDrawable(getResources().getDrawable(R.drawable.my));
                        tv_my.setTextColor(unPressedColor);
                        break;

                }
                switch (position) {
                    case 0:
                        iv_home.setImageDrawable(getResources().getDrawable(R.drawable.home_pressed));
                        tv_home.setTextColor(pressedColor);
                        break;
                    case 1:
                        iv_discuss.setImageDrawable(getResources().getDrawable(R.drawable.discuss_pressed));
                        tv_discuss.setTextColor(pressedColor);
                        break;
                    case 2:
                        iv_sign.setImageDrawable(getResources().getDrawable(R.drawable.sign_pressed));
                        tv_sign.setTextColor(pressedColor);
                        break;
                    case 3:
                        iv_my.setImageDrawable(getResources().getDrawable(R.drawable.my_pressed));
                        tv_my.setTextColor(pressedColor);
                        break;
                }
                currIndex = position;
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_home:
                mPager.setCurrentItem(0, false);
                break;
            case R.id.btn_discuss:
                mPager.setCurrentItem(1, false);
                break;
            case R.id.btn_sign:
                mPager.setCurrentItem(2, false);
                break;
            case R.id.btn_my:
                mPager.setCurrentItem(3, false);
                break;
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            moveTaskToBack(true);
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        moveTaskToBack(true);
    }
}
