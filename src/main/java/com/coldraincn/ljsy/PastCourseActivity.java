package com.coldraincn.ljsy;

import android.os.Bundle;
import android.widget.ListView;

import com.coldraincn.ljsy.adapter.PastCourseAdapter;
import com.coldraincn.ljsy.bean.Course;

import java.util.ArrayList;
import java.util.List;

public class PastCourseActivity extends BaseActivity {
    private ListView listView;
    List<Course> mDatas = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_past_course);
        listView = (ListView) findViewById(R.id.listView);
        for (int i = 0; i < 15; i++) {
            mDatas.add(null);
        }
        listView.setAdapter(new PastCourseAdapter(this, mDatas));
    }
}
