package com.coldraincn.ljsy;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.coldraincn.ljsy.util.GlideImageLoader;
import com.youth.banner.Banner;
import com.youth.banner.BannerConfig;
import com.youth.banner.Transformer;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class CourseDetailActivity extends BaseActivity {
    private TextView tv_hj;
    private TextView tv_price;
    private TextView tv_commit;
    private TextView tv_course;
    private View iv_shuka;
    private View iv_share;
    private boolean isMine;
    private Banner banner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_course_detail);
        tv_hj = (TextView) findViewById(R.id.tv_hj);
        tv_price = (TextView) findViewById(R.id.tv_price);
        tv_commit = (TextView) findViewById(R.id.tv_commit);
        tv_course = (TextView) findViewById(R.id.tv_course);
        iv_shuka = findViewById(R.id.iv_shuka);
        iv_share = findViewById(R.id.iv_share);
        banner = (Banner) findViewById(R.id.banner);
        isMine = getIntent().getBooleanExtra("isMine", false);
        if (isMine) {
            tv_hj.setVisibility(View.GONE);
            tv_price.setVisibility(View.GONE);
            tv_commit.setVisibility(View.VISIBLE);
            tv_course.setText("往期课程");
            iv_shuka.setVisibility(View.VISIBLE);
            tv_commit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(CourseDetailActivity.this, MyhomeWorkActivity.class);
                    startActivity(intent);
                }
            });
            tv_course.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(CourseDetailActivity.this, PastCourseActivity.class);
                    startActivity(intent);
                }
            });
        } else {
            tv_course.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(CourseDetailActivity.this, PayActivity.class);
                    startActivity(intent);
                }
            });
        }
        List images=new ArrayList<>();
        images.add(R.mipmap.aaa);
        images.add(R.mipmap.bbb);
       // Integer[] images={R.mipmap.aaa,R.mipmap.bbb};

            //设置图片加载器
            banner.setImageLoader(new GlideImageLoader());
            //设置图片集合
            banner.setImages(images);
            banner.setBannerAnimation(Transformer.DepthPage);
            //设置自动轮播，默认为true
            banner.isAutoPlay(true);
            //设置轮播时间
            banner.setDelayTime(5000);
            //设置指示器位置（当banner模式中有指示器时）
            banner.setIndicatorGravity(BannerConfig.CENTER);
            //banner设置方法全部调用完毕时最后调用
            banner.start();
    }
}
