package com.coldraincn.ljsy.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;


/**
 * @author: 王金强
 * @version:
 * @Date: 2017年4月14日19:48:16
 */
public class PreferenceUtils {
    private static final String PREFERENCE_NAME = "USER";
    private static SharedPreferences mSharedPreferences;
    private static PreferenceUtils mPreferenceUtils;
    private static SharedPreferences.Editor editor;


    private static final String phone = "phone";//
    private static final String token = "token";
    private static final String communityOid = "communityOid";
    private static final String userRole = "userRole";
    private static final String comUserName = "comUserName";//
    private static final String comUserHeadImg = "comUserHeadImg";//
    private static final String userSex = "userSex";//
    private static final String userWxCity = "userWxCity";//
    private static final String userWxCountry = "userWxCountry";//
    private static final String userWxHeadimgurl = "userWxHeadimgurl";//
    private static final String userWxNickname = "userWxNickname";//
    private static final String userWxProvince = "userWxProvince";//
    private static final String vipcard = "vipcard";//
    private static final String communityName = "communityName";//


    private PreferenceUtils(Context cxt) {
        mSharedPreferences = cxt.getSharedPreferences(PREFERENCE_NAME, Context.MODE_PRIVATE);
        editor = mSharedPreferences.edit();
    }

    public static synchronized void init(Context cxt) {
        if (mPreferenceUtils == null) {
            mPreferenceUtils = new PreferenceUtils(cxt);
        }
    }

    public static PreferenceUtils getInstance() {
        if (mPreferenceUtils == null) {
            throw new RuntimeException("please init first!");
        }

        return mPreferenceUtils;
    }

    /**
     * 退出的时候调用一下这个方法，清空SP
     */
    private void removeCurrentUserInfo() {
        editor.remove(communityOid);
        editor.remove(phone);
        editor.remove(token);
        editor.remove(userRole);
        editor.commit();
        // editor.clear().commit();
    }


    public void setCommunityOid(String number) {
        editor.putString(communityOid, number);
        editor.commit();
    }

    public String getCommunityOid() {
        return mSharedPreferences.getString(communityOid, "");
    }

    public void setVipcard(int number) {
        editor.putInt(vipcard, number);
        editor.commit();
    }

    public int getVipcard() {
        return mSharedPreferences.getInt(vipcard, 0);
    }

    public void setUserWxProvince(String number) {
        editor.putString(userWxProvince, number);
        editor.commit();
    }

    public String getUserWxProvince() {
        return mSharedPreferences.getString(userWxProvince, "");
    }

    public void setUserWxNickname(String number) {
        editor.putString(userWxNickname, number);
        editor.commit();
    }

    public String getUserWxNickname() {
        return mSharedPreferences.getString(userWxNickname, "");
    }


    public void setUserWxHeadimgurl(String number) {
        editor.putString(userWxHeadimgurl, number);
        editor.commit();
    }

    public String getUserWxHeadimgurl() {
        return mSharedPreferences.getString(userWxHeadimgurl, "");
    }


    public void setUserWxCountry(String number) {
        editor.putString(userWxCountry, number);
        editor.commit();
    }

    public String getUserWxCountry() {
        return mSharedPreferences.getString(userWxCountry, "");
    }


    public void setUserWxCity(String number) {
        editor.putString(userWxCity, number);
        editor.commit();
    }

    public String getUserWxCity() {
        return mSharedPreferences.getString(userWxCity, "");
    }


    public void setUserSex(String number) {
        editor.putString(userSex, number);
        editor.commit();
    }

    public String getUserSex() {
        return mSharedPreferences.getString(userSex, "");
    }


    public void setComUserName(String number) {
        editor.putString(comUserName, number);
        editor.commit();
    }

    public String getComUserName() {
        return mSharedPreferences.getString(comUserName, "");
    }


    public void setComUserHeadImg(String number) {
        editor.putString(comUserHeadImg, number);
        editor.commit();
    }

    public String getComUserHeadImg() {
        return mSharedPreferences.getString(comUserHeadImg, "");
    }


    public void setCommunityName(String number) {
        editor.putString(communityName, number);
        editor.commit();
    }

    public String getCommunityName() {
        return mSharedPreferences.getString(communityName, "");
    }


    public void setToken(String name) {
        editor.putString(token, name);
        editor.commit();
    }

    public String getToken() {
        return mSharedPreferences.getString(token, "");
    }

    public void setUserRole(int name) {
        editor.putInt(userRole, name);
        editor.commit();
    }

    public int getUserRole() {
        return mSharedPreferences.getInt(userRole, 1);
    }


    public void setPhone(String temp) {
        editor.putString(phone, temp);
        editor.commit();
    }

    public String getPhone() {
        return mSharedPreferences.getString(phone, "");
    }

    private String jiami(String pwd) {
        if (TextUtils.isEmpty(pwd)) {
            return "";
        }
        String str = "";
        for (int a = 0; a < pwd.length(); a++) {
            str += (char) (pwd.charAt(a) ^ a);
        }
        return str;

    }


}
