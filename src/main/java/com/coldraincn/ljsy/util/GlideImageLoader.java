package com.coldraincn.ljsy.util;

/**
 * Created by hd on 2017/9/29.
 */

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.widget.ImageView;


import com.youth.banner.loader.ImageLoader;

/**
 * Created by hd on 2017/9/8.
 */

public class GlideImageLoader extends ImageLoader {
    @Override
    public void displayImage(Context context, Object path, ImageView imageView) {
        /**
         注意：
         1.图片加载器由自己选择，这里不限制，只是提供几种使用方法
         2.返回的图片路径为Object类型，由于不能确定你到底使用的那种图片加载器，
         传输的到的是什么格式，那么这种就使用Object接收和返回，你只需要强转成你传输的类型就行，
         切记不要胡乱强转！
         */




        //Picasso 加载图片简单用法
//        Picasso.with(context).load(path.toString()).into(imageView);
//        DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true).cacheOnDisk(true)
//                .showImageForEmptyUri(R.drawable.banner_default).showImageOnFail(R.drawable.banner_default)
//                .showImageOnLoading(R.drawable.banner_default)
//                .imageScaleType(ImageScaleType.EXACTLY)
//                .bitmapConfig(Bitmap.Config.RGB_565).build();
        ImageLoaderUtil.displayImage(context, imageView, path.toString(), ImageLoaderUtil.getPhotoImageOption(), null);


    }




}
